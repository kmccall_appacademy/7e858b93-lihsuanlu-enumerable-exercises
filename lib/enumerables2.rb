require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) do |sum,num|
    sum += num
  end
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all?{|longstring| sub_tring?(longstring,substring)}
end

def sub_tring?(longstring,substring)
  longstring.include?(substring)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  ans = []
  ('a'..'z').to_a.each do |letter|
    if string.delete(letter).length < string.length - 1
      ans << letter
    end
  end
  ans
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string = string.split
  arr = []
  arr << longest_word(string)
  string.delete(arr[0])
  arr << longest_word(string)
  arr
end

def longest_word(arr)
  longest_length = 0
  longest_len_index = 0
  arr.each_with_index do |word, index|
    if word.length > longest_length
      longest_length = word.length
      longest_len_index = index
    end
  end
  arr[longest_len_index]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  string = string.chars.uniq.sort
  ('a'..'z').to_a.reduce([]) do |arr,letter|
    if !string.include?(letter)
      arr << letter
    else
      arr
    end
  end
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).reduce([]) do |arr,year|
    if not_repeat_year?(year)
      arr << year
    else
      arr
    end
  end
end

def not_repeat_year?(year)
  year = year.to_s.chars.map(&:to_i).sort
  year[0...-1].each_index do |index|
    return false if year[index] == year[index+1]
  end
  return true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  new_arr = songs
  songs.uniq.reduce([]) do |arr,song|
    if no_repeats?(song,new_arr)
      arr << song
    else
      arr
    end
  end

end

def no_repeats?(song_name, songs)
  songs[0...-1].each_index do |index|
    if songs[index] == song_name && songs[index+1] == song_name
      return false
    end
  end
  return true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  shortest_cdist = string.length
  string = string.gsub(/[^a-z0-9\s]/i, '').split
  cdist_word = ""
  string.each do |word|
    cdist_cur = c_distance(word)
    if cdist_cur < shortest_cdist && cdist_cur < word.length
      shortest_cdist = c_distance(word)
      cdist_word = word
    end
  end
  cdist_word
end

def c_distance(word)
  word.chars.each_index{|index| return index if word[index] == 'c'}
  return word.length
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  ans = []
  index = 0
  while index < arr.length-1
    if arr[index] == arr[index+1]
      ans << number_range(arr,index)
      index = ans[-1][1]
    end
    index += 1
  end
  ans

end

def number_range(arr,start_index)
  index = start_index
  while arr[index] == arr[index+1]
    index += 1
  end
  return [start_index,index]
end
